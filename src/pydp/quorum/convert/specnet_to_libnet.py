import argparse
from pyteomics import mgf
import sys
import logging

def convert(args):
   pass 


if __name__ == '__main__':
   parser = argparse.ArgumentParser(description="Converting a ")
   parser.add_argument("lib_mgf", type=str, help="mgf file containing library spectra")
   parser.add_argument("edges_tsv", type=str, help="edges files produced by network analysis of library spectra")
   parser.add_argument("-o", dest="output", type=str, default=None, help="output for new edges file.")
   args = parser.parse_args()
   convert(args)
